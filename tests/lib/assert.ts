type AssertionError = {
    assertion: string;
    message: string;
};

const assert = (b: boolean, err: AssertionError) => {
    if (!b) {
        const error = new Error(`    in the assertion ${err.assertion}()\n${err.message}`);
        error.name = "assertion";
        throw error;
    }
}

const nearFloat = (a: number, b: number, precision?: number) => {
    if (precision === undefined || precision === 0) {
        precision = 1e-4;
    }
    assert(
        Math.abs(a - b) < precision,
        {assertion: "nearFloat", message: `\tExpected: ${a} ± ${precision.toExponential(1)}\n\tReceived: ${b}`}
    );
};

const same = (a: any, b: any) => assert(
    a === b,
    {assertion: "same", message: `Expected: ${a}\nReceived: ${b}\n`}
);

export default {
    nearFloat,
    same,
}
