import test from './lib/test'
import assert from './lib/assert'

import * as LA from '../src/LinearAlgebra'

import testLinearAlgebra from './series/linearalgebra'
import testExact from './series/exact'
import testNist from './series/nist'
import testRealworld from './series/realworld'

testLinearAlgebra();
testExact();
testNist();
testRealworld();
