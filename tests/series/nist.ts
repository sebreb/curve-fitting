import dataNist from '../data/nist'
import assert from '../lib/assert'
import { levenbergMarquardt } from '../../src/main'
import test from '../lib/test'

export default function() {
    for (const tcase of dataNist) {
        const results = levenbergMarquardt(tcase.data, tcase.modelFunction, tcase.options);
        test(tcase.name, () => {
            assert.nearFloat(tcase.expected.error, results.error, 1e-1);
            /*
            for (let i = 0; i < tcase.expected.parameters.length; i++) {
                assert.nearFloat(tcase.expected.parameters[i], results.parameters[i], 0.1);
            }
            */
        })
        console.log("\tIterations", results.iterations, "error", results.error);
    }
}
