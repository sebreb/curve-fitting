import dataExact from '../data/exact'
import assert from '../lib/assert'
import { InputData } from '../../src/types'
import { levenbergMarquardt } from '../../src/main'
import test from '../lib/test'

function computeInputData(tcase: any): InputData {
    const data: InputData = {
        x: new Float64Array(tcase.data.steps),
        y: new Float64Array(tcase.data.steps),
    }
    const targetFunction = tcase.modelFunction(tcase.expected.parameters);
    const width = tcase.data.xmax - tcase.data.xmin;
    for (let i = 0; i < tcase.data.steps; i++) {
        let x = tcase.data.xmin + i * width / tcase.data.steps;
        data.x[i] = x;
        data.y[i] = targetFunction.call(this, x);
    }
    return data;
}

export default function() {
    for (const tcase of dataExact) {
        const data = computeInputData(tcase);
        const results = levenbergMarquardt(data, tcase.modelFunction, tcase.options);
        test(tcase.name, () => {
            assert.nearFloat(0, results.error);
            for (let i = 0; i < tcase.expected.parameters.length; i++) {
                assert.nearFloat(tcase.expected.parameters[i], results.parameters[i], 0.1);
            }
        })
        console.log("\tIterations", results.iterations, "error", results.error);
    }
}
