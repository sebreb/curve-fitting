import assert from '../lib/assert'
import test from '../lib/test'
import * as LA from '../../src/LinearAlgebra'

export default function() {
    test("Invert a matrix", () => {
        const m = new LA.Matrix(3, 3, [2, 1, -1, -3, -1, 2, -2, 1, 2]);
        const v = new Float64Array([8, -11, -3]);
        const result = LA.solve(m, v);
        assert.same(3, result.length);
        assert.same(2, result[0]);
        assert.same(3, result[1]);
        assert.same(-1, result[2]);
    });

    test("Invert a matrix (swapping rows)", () => {
        const m = new LA.Matrix(3, 3, [0, 0, -1, 0, -1, 2, 4, 1, 2]);
        const v = new Float64Array([3, -7, 3]);
        const result = LA.solve(m, v);
        assert.same(3, result.length);
        assert.same(2, result[0]);
        assert.same(1, result[1]);
        assert.same(-3, result[2]);
    });
}
