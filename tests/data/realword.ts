const inverseData = {
    x: [
        100, 200, 300, 400, 500, 600, 700, 800, 900, 1000
    ],
    y: [
        1557.69230769231, 786.764705882353, 517.441860465116, 401.041666666667 , 323.529411764706, 273.584905660377, 231.818181818182, 202.702702702703, 174.107142857143, 163.716814159292
    ],
};

export default [
    {
        name: 'simple inverse',
        modelFunction:
            ([b1]) =>
                (x) =>
                    10 * b1 / x,
        data: inverseData,
        expected: {
            error: 623.577,
            parameters: [15668.739616374163],
        },
        options: {
            parameters: {
                initial: [1],
            },
        },
    },
    {
        name: 'unstable inverse',
        modelFunction:
            ([b1]) =>
                (x) =>
                    10 / (b1 * x),
        data: inverseData,
        expected: {
            error: 623.577,
            parameters: [6.38213e-5],
        },
        options: {
            parameters: {
                initial: [2e-4],
            },
        },
    },
]
