function sumOfLorentzians(p) {
    return (t) => {
        let nL = p.length;
        let factor, p2;
        let result = 0;
        for (let i = 0; i < nL; i += 3) {
            p2 = Math.pow(p[i + 2] / 2, 2);
            factor = p[i + 1] * p2;
            result += factor / (Math.pow(t - p[i], 2) + p2);
        }
        return result;
    };
}

export default [
    {
        name: 'bennet5([2, 3, 5])',
        modelFunction([b1, b2, b3]) {
            return (t) => b1 * Math.pow(t + b2, -1 / b3);
        },
        data: {
            steps: 154,
            xmin: -2.6581,
            xmax: 49.6526,
        },
        expected: {
            parameters: [2, 3, 5],
        },
        options: {
            damping: 0.00001,
            maxIterations: 10,
            errorTolerance: 1e-7,
            parameters: {
                initial: [3.5, 3.8, 4],
                max: [11, 11, 11],
                min: [1, 2.7, 1],
            }
        },
    },
    {
        name: '2*sin(2*t)',
        modelFunction([a, b]) {
            return (t) => a * Math.sin(b * t);
        },
        data: {
            steps: 20,
            xmin: 0,
            xmax: 6,
        },
        expected: {
            parameters: [2, 2],
        },
        options: {
            maxIterations: 100,
            //parametersDelta: [10e-4],
            //damping: 0.1,
            parameters: { initial: [3, 3] },
        },
    },
    {
        name: 'Sigmoid',
        modelFunction([a, b, c]) {
            return (t) => a / (b + Math.exp(-t * c));
        },
        data: {
            steps: 20,
            xmin: 0,
            xmax: 19,
        },
        expected: {
            parameters: [2, 2, 2],
        },
        options: {
            damping: 0.1,
            parameters: { initial: [3, 3, 3] },
            maxIterations: 200,
        },
    },
    {
        name: 'Sum of lorentzians',
        modelFunction: sumOfLorentzians,
        data: {
            steps: 100,
            xmin: 0,
            xmax: 99,
        },
        expected: {
            parameters: [1.05, 0.1, 0.3, 4, 0.15, 0.3],
        },
        options: {
            damping: 0.01,
            parametersDelta: [0.01, 0.0001, 0.0001, 0.01, 0.0001, 0.001],
            parameters: { initial: [1.1, 0.15, 0.29, 4.05, 0.17, 0.28] },
            maxIterations: 500,
        },
    },
    {
        name: 'Sum of lorentzians 2',
        modelFunction: sumOfLorentzians,
        data: {
            steps: 100,
            xmin: 0,
            xmax: 99,
        },
        expected: {
            parameters: [1.05, 0.1, 0.3, 4, 0.15, 0.3],
        },
        options: {
            parameters: { initial: [1.1, 0.15, 0.27, 4.5, 0.2, 0.4] },
        },
    },
];
