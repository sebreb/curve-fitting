class Matrix {
    rows: number;
    columns: number;
    values: Float64Array;

    constructor(rows: number, columns: number, values?: number[]) {
        this.rows = rows;
        this.columns = columns;
        if (values) {
            this.values = new Float64Array(values);
        } else {
            this.values = new Float64Array(rows * columns);
            this.values.fill(0);
        }
    }
}

function norm1(m: Matrix): number {
    let max = 0;
    for (let i = 0; i < m.values.length; i++) {
        max = Math.max(max, Math.abs(m.values[i]));
    }
    return max;
}

const almostZero = 1e-20;

/**
 * Return the vector X such that M X = V.
 */
function solve(m: Matrix, v: Float64Array): Float64Array {
    // Concat the matrix M and the vector V.
    if (m.rows !== v.length) {
        throw new Error("Cannot solve because of wrong dimensions.");
    }
    const a = new Float64Array(m.rows * (1 + m.columns));
    let p = 0;
    let iv = 0;
    for (let im = 0; im < m.rows * m.columns; im++) {
        a[p] = m.values[im];
        p++;
        if (im % m.columns === m.columns - 1) {
            a[p] = v[iv];
            p++;
            iv++;
        }
    }

    // Gauss-Jordan elimination
    const numRows = m.rows;
    const numColumns = m.columns + 1;
    const size = m.rows * m.columns;
    for (let i = 0; i < numRows; i++) {
        const pivot = i * (numColumns + 1);
        // Ensure that the pivot cell is not zero.
        if (Math.abs(a[pivot]) < almostZero) {
            let swapped = false;
            for (let j = pivot + numColumns; j < size; j += numColumns) {
                if (Math.abs(a[j]) > almostZero) {
                    // Swap the rows
                    const row = a.slice(j, j + numColumns - i);
                    for (let k = 0; k < numColumns - i; k++) {
                        a[j + k] = a[pivot + k];
                        a[pivot + k] = row[k];
                    }
                    swapped = true;
                    break;
                }
            }
            if (!swapped) {
                console.error(a);
                throw new Error("No solution.");
            }
        }

        // 1 on the diagonal
        const c = a[pivot];
        a[pivot] = 1;
        for (let k = 1; k < numColumns - i; k++) {
            a[pivot + k] /= c;
        }

        // Substract this row to others
        for (let r = 0; r < numRows; r++) {
            if (r === i || Math.abs(a[r * numColumns + i]) < almostZero) {
                continue;
            }
            const sub = a[r * numColumns + i];
            a[r * numColumns + i] = 0;
            for (let k = 1; k < numColumns - i; k++) {
                a[r * numColumns + i + k] -= sub * a[pivot + k];
            }
        }
    }

    const x = new Float64Array(v.length);
    for (let i = 0; i < x.length; i++) {
        x[i] = a[numColumns - 1 + i * numColumns];
    }
    return x;
}

export {
    Matrix,
    norm1,
    solve,
}
