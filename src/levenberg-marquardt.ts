import * as LA from './LinearAlgebra'
import * as LM from './types'

const epsilonStep = 1e-1;
const epsilonGradient = 1e-8;
const epsilonParameters = 1e-8;
const epsilonChi2red = 1e-16;

const dampingStepUp = 11;
const dampingStepDown = 9;

/**
 * Find the optimal parameters so that the model function fits the data points.
 *
 * More precisely, apply the Levenberg-Marquardt algorithm,
 * to find a local optimum of the parameters of the model function,
 * so that the least-squares error to the data is minimal (best fit).
 */
function levenbergMarquardt(input: LM.InputData, fn: LM.ModelFunction, o: Partial<LM.Options>): LM.Result {
    const options = completeOptions(o, input);
    const numParameters = options.parameters.initial.length;
    const numPoints = input.x.length;
    if (input.x.length !== input.y.length) {
        throw new Error("The arrays input.x and input.y have different sizes.");
    }
    if (input.x.length <= numParameters) {
        throw new Error("Fitting requires more input points than parameters in the model function.");
    }

    let parameters = options.parameters.initial.slice(0);
    let parametersOld = parameters.slice(0);

    let yOld = new Float64Array(numPoints); // i.e. modelValues : eval of `modelFunction` for `input.x`
    //let y = new Float64Array(input.x.map(fn(Array.from(parameters))));
    let y = new Float64Array(input.x.length);
    let fnp = fn(Array.from(parameters));
    for (let i = 0; i < y.length; i++) {
        y[i] = fnp.call(null, input.x[i]);
    }

    const computeNext = new ComputeNext(input, fn, options);

    const jacobian = new LA.Matrix(numPoints, numParameters); // AKA J
    const JtWJ = new LA.Matrix(numParameters, numParameters);
    const JtWdy = new Float64Array(numParameters);
    computeNext.updateJacobian(jacobian, parameters, y);
    computeNext.updateJtWJ(JtWJ, jacobian);
    computeNext.updateJtWdy(JtWdy, jacobian, y);

    let chi2 = computeNext.computeChi2(y);
    let chi2Old = 0;

    let lambda = options.damping;
    let converged = false;

    let iteration = 0;
    while (!converged && iteration < options.maxIterations) {
        iteration++;

        // Change parameters
        const h = computeParametersDelta(lambda, JtWJ, JtWdy)
        parametersOld = parameters.slice(0);
        for (let i = 0; i < numParameters; i++) {
            parameters[i] += h[i];
            // Apply parameter bounds
            if (options.parameters.min && parameters[i] < options.parameters.min[i]) {
                parameters[i] = options.parameters.min[i];
            }
            if (options.parameters.max && parameters[i] > options.parameters.max[i]) {
                parameters[i] = options.parameters.max[i];
            }
        }

        // Evaluate the model function
        yOld = y.slice(0);
        y = new Float64Array(input.x.map(fn(Array.from(parameters))));

        // Compute the error
        chi2Old = chi2;
        chi2 = computeNext.computeChi2(y);

        // Is this an successful step?
        const rho = (chi2Old - chi2) / computeNext.computeRhoDenominator(h, lambda, JtWdy);
        //console.log("H0", h[0], "CHI2", chi2, "rho", rho, "lambda", lambda);
        if (rho > epsilonStep) {
            //console.log("P", parameters);
            // The new parameters are significantly better.
            computeNext.updateJacobian(jacobian, parameters, y);
            computeNext.updateJtWJ(JtWJ, jacobian);
            computeNext.updateJtWdy(JtWdy, jacobian, y);
        
            // Decrease lambda ⇒ toward a Gauss-Newton method
            lambda = Math.max(lambda / dampingStepDown, 1e-20);
        } else {
            // Revert to the old parameters.
            parameters = parametersOld.slice(0);
            y = yOld.slice(0);
            chi2 = chi2Old;

            // Increase lambda ⇒ toward a gradient descent
            lambda = Math.min(lambda * dampingStepUp, 1e20);

            // Do not test for convergence, because this step failed.
            continue;
        }

        // Test for convergence
        if (iteration <= 2) {
            continue;
        }
        if (LA.norm1(JtWJ) < epsilonGradient) {
            // Convergence in gradient
            //console.log("Convergence in gradient")
            converged = true;
        } else if (chi2 / (input.x.length - numParameters) < epsilonChi2red) {
            // Convergence in error (reduced chi2)
            //console.log("Convergence in chi2")
            converged = true;
        } else {
            let hMax = 0;
            for (let i = 0; i < numParameters; i++) {
                hMax = Math.max(hMax, Math.abs(h[i]));
            }
            if (hMax < epsilonParameters) {
                //console.log("Convergence in parameters")
                converged = true;
            }
        }
    }

    return {
        error: chi2,
        iterations: iteration,
        parameters: Array.from(parameters),
    };
}

/**
 * Return the vector H that solves : (J^T W J + λ I) H = J^T W (Y - Ŷ).
 */
const computeParametersDelta = (lambda: number, JtWJ: LA.Matrix, JtWdy: Float64Array): Float64Array => {
    // Add λI to J^T W J
    for (let i = 0; i < JtWJ.rows; i++) {
        JtWJ.values[i * JtWJ.columns + i] += lambda;
    }
    // Solve the linear equations.
    return LA.solve(JtWJ, JtWdy);
}

class ComputeNext {
    #input: LM.InputData
    #modelFunction: LM.ModelFunction
    #options: Options
    constructor(input: LM.InputData, modelFunction: LM.ModelFunction, o: Options) {
        this.#input = input;
        this.#modelFunction = modelFunction;
        this.#options = o;
    }
    computeChi2(y: Float64Array): number {
        let sum = 0;
        for (let i = 0; i < this.#input.y.length; i++) {
            let sq = Math.pow(this.#input.y[i] - y[i], 2);
            if (this.#options.weights.length > 0) {
                sq *= this.#options.weights[i];
            }
            sum += sq;
        }
        return sum;
    }
    computeRhoDenominator(h: Float64Array, lambda: number, JtWdy: Float64Array): number {
        // |dotproduct(h, λ h + JtWdy)|
        let sum = 0;
        for (let i = 0; i < h.length; i++) {
            sum += h[i] * (lambda * h[i] + JtWdy[i]);
        }
        return Math.abs(sum);
    }
    updateJacobian(J: LA.Matrix, params: Float64Array, y: Float64Array): void {
        const x = Array.from(this.#input.x);
        for (let j = 0; j < J.columns; j++) {
            const pRight = Array.from(params);
            const pLeft = Array.from(params);
            pRight[j] = pRight[j] + this.#options.parametersDelta[j];
            pLeft[j] = pRight[j] - this.#options.parametersDelta[j];
            for (let i = 0; i < J.rows; i++) {
                const dyRight = this.#modelFunction(pRight).call(this, x[i]) - y[i];
                if (Math.abs(dyRight) > this.#options.parametersDelta[j] / 500) {
                    // Forward differences (faster)
                    J.values[i * J.columns + j] = dyRight / this.#options.parametersDelta[j];
                } else {
                    // Central differences (slower, but the precision is required here)
                    const dyLeft = y[i] - this.#modelFunction(pLeft).call(this, x[i]);
                    J.values[i * J.columns + j] = 0.5 * (dyLeft + dyRight) / this.#options.parametersDelta[j];
                }
            }
        }
    }
    updateJtWJ(JtWJ: LA.Matrix, jacobian: LA.Matrix): void {
        for (let i = 0; i < JtWJ.rows; i++) { // 0 .. numParameters-1
            for (let j = 0; j < JtWJ.columns; j++) { // 0 .. numParameters-1
                // JtWJ[i,j] = weight[j] * dotproduct(col i of J, col j of J)
                let sum = 0;
                for (let k = 0; k < jacobian.rows; k++) {
                    sum += jacobian.values[k * jacobian.columns + i] * jacobian.values[k * jacobian.columns + j];
                }
                if (this.#options.weights.length > 0) {
                    sum *= this.#options.weights[j];
                }
                JtWJ.values[i * JtWJ.columns + j] = sum;
            }
        }
    }
    updateJtWdy(JtWdy: Float64Array, jacobian: LA.Matrix, y: Float64Array): void {
        for (let j = 0; j < JtWdy.length; j++) {
            // JtWdy[j] = weight[j] * dotproduct(col j of J, input.y - y)
            let sum = 0;
            for (let k = 0; k < jacobian.rows; k++) {
                sum += jacobian.values[k * jacobian.columns + j] * (this.#input.y[k] - y[k]);
            }
            if (this.#options.weights.length > 0) {
                sum *= this.#options.weights[j];
            }
            JtWdy[j] = sum;
        }
    }
}

interface Options {
    damping: number;
    errorTolerance: number;
    parametersDelta: Float64Array;
    maxIterations: number;
    parameters: {
        initial: Float64Array;
        max: null | Float64Array;
        min: null | Float64Array;
    };
    timeout: number;
    weights: Float64Array;
}

function completeOptions(o: Partial<LM.Options>, input: LM.InputData): Options {
    if (!Array.isArray(o?.parameters?.initial) || o?.parameters?.initial.length === 0) {
        throw new Error("You must pass an array of numbers into the option 'parameters.initial'.");
    }
    const numParameters = o.parameters.initial.length;

    let parametersDelta;
    if (Array.isArray(o?.parametersDelta) && o.parametersDelta.length > 0) {
        if (o.parametersDelta.length === numParameters) {
            parametersDelta = new Float64Array(o.parametersDelta);
        } else if (o.parametersDelta.length === 1 && typeof o.parametersDelta[0] === 'number') {
            parametersDelta = new Float64Array(numParameters);
            parametersDelta.fill(o.parametersDelta[0]);
        } else {
            throw new Error("Incorrect value for parametersDelta");
        }
    } else if (!o?.parametersDelta) {
        const averageInterval = (input.x[input.x.length - 1] - input.x[0]) / input.x.length;
        if (averageInterval < 0) {
            throw new Error("Input data x must be sorted by ascending values.");
        }
        parametersDelta = new Float64Array(numParameters);
        parametersDelta.fill(averageInterval / 100);
    } else {
        throw new Error("Incorrect value for parametersDelta");
    }

    let weights;
    if (o.weights) {
        weights = new Float64Array(o.weights);
    } else {
        weights = new Float64Array();
    }

    return {
        damping: (typeof o?.damping === 'number' && o.damping > 0 ? o.damping : 2),
        errorTolerance: o?.errorTolerance || 1e-3,
        parametersDelta,
        maxIterations: (typeof o?.maxIterations === 'number' && o.maxIterations > 0 ? o.maxIterations : 40 * numParameters),
        parameters: {
            initial: new Float64Array(o.parameters.initial),
            max: o.parameters?.max ? new Float64Array(o.parameters.max) : null,
            min: o.parameters?.min ? new Float64Array(o.parameters.min) : null,
        },
        timeout: o?.timeout || 0,
        weights,
    };
}

export default levenbergMarquardt
