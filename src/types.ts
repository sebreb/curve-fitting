// public types
type Fitting = (d: InputData, fn: ModelFunction, o?: Partial<Options>) => Result;

/**
 * Function that will fit into the points.
 */
type ModelFunction = (parameters: number[]) => (x: number) => number;

/**
 * Coordinates of the points to fit.
 */
interface InputData {
    x: number[] | Float64Array;
    y: number[] | Float64Array;
}

interface Options {
    /**
     * The Levenberg-Marquardt initial lambda parameter, AKA λ₀.
     * It's usually recommended to set an initial value > 1.
     * Default value: 2
     */
    damping?: number;

    /**
     * The step size to approximate the jacobian matrix with finite differences.
     * If too large, the estimation of the derivation (relative to a parameter) will be imprecise.
     * The format is either a single number, or a number for each parameter.
     * Default value: (xmax -xmin) / x.length / 100
     */
    parametersDelta?: number[];

    /**
     * The maximum number of iterations before halting.
     * Default value: 40 * parameters.length
     */
    maxIterations?: number;

    parameters: {
        /**
         * REQUIRED.
         * Initial guesses for the parameters.
         */
        initial: number[];

        /**
         * Optional. Maximum values for each of the parameters.
         */
        max?: number[];

        /**
         * Optional. Minimum values for each of the parameters.
         */
        min?: number[];
    }

    /**
     * Weighting vector. Empty array, or with the same length of the input data.
     * Usually, the weight in x_i is the square inverse of the uncertainty at x_i.
     * E.g.  [y_i^-2]  for an uncertainty proportional to the value at x_i.
     * Default value: [] (same weight on every point)
     */
    weights?: number[];
}

interface Result {
    /**
     * The weighted least-squares error, AKA χ² = Σ (y_i - f_p(x_i))² * w_i
     */
    error: number;

    /**
     * Nυmber of iterations of the LM algorithm.
     */
    iterations: number;

    /**
     * Final parameters of the model function.
     */
    parameters: number[];
}

export {
    ModelFunction,
    InputData,
    Result,
    Options,
}
