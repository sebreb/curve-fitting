Curve fitting
=============

Given some points in R×R and a parametered function f : R → R,
find an optimal set of parameters so that f "fits" the points.

The algorithm implemented is Levenberg-Marquardt.
It is suitable for non-linear least-square approximation.


Usage
-----

### Loading the library

In a JS web code, load `build/web/curve-fitting.js`.
This file is on the `dist` branch, but you can also build it from the master branch.

In a JS node code, import `build/module/curve-fitting.js`.

In a TypeScript code, you can import `src/main.ts`.

### Practical examples

Web (global variable)
```
<script src="curve-fitting.js"></script>
<script>
const data = {
    x: [-5, -1.0, 1, 4, 5],
    y: [28.7, 3.2, 3.1, 18.8, 29],
};
const modelFunction = ([a, b]) => {
    return (x) => a * x * x + b;
};
const results = CurveFitting.levenbergMarquardt(
    data,
    modelFunction,
    {
        parameters: { initial: [4, 4] }
    }
);
console.log(results);
</script>
```

Node or web (module)
```
<script type="module">
import {levenbergMarquardt} from 'build/module/curve-fitting.js';
const result = levenbergMarquardt(…);
```

Building
--------

Replace `pnpm` with `npm` or whatever is your favorite JS package manager.
```sh
pnpm install
pnpm build
```

Dev
---

```sh
pnpm install
# In terminal 1
pnpm watch
# In terminal 2
…modify the code
pnpm test
```

TODO
----

Fix the algorithm implementation for the NIST tests that fail (7/15).

Benchmarks.

More documentation.
